import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");

describe('Bonus tests', () => {
    let app;
    console.log("Tests started");


    test('Valid: program and amount(<10000)',  (done) => {
        let program = "Standard"
        let amount = 1000
        assert.equal(calculateBonuses(program, amount), 0.05);
        done();
    });

    test('Valid: program and amount(10000)',  (done) => {
        let program = "Premium"
        let amount = 10000
        assert.equal(calculateBonuses(program, amount), 1.5 * 0.1);
        done();
    });

    test('Valid: program and amount(50000)',  (done) => {
        let program = "Diamond"
        let amount = 50000
        assert.equal(calculateBonuses(program, amount),2 * 0.2);
        done();
    });

    test('Valid: program and amount(100000)',  (done) => {
        let program = "Premium"
        let amount = 100000
        assert.equal(calculateBonuses(program, amount),2.5 * 0.1);
        done();
    });

    test('Valid: program and amount(>100000)',  (done) => {
        let program = "Premium"
        let amount = 200000
        assert.equal(calculateBonuses(program, amount),2.5 * 0.1);
        done();
    });

    test('Invalid program', (done) => {
        let program = "Pre"
        let amount = 100000
        assert.equal(calculateBonuses(program, amount),0);
        done();
    });

    test('Invalid: program and amount', (done) => {
        let program = false
        let amount = false
        assert.equal(calculateBonuses(program, amount),0);
        done();
    });

    test('Empty values', (done) => {
        let program = ""
        let amount = ""
        assert.equal(calculateBonuses(program, amount),0);
        done();
    });


    console.log('Tests Finished');

});
